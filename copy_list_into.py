from files import Files


def get_list_of_files_sorted_by_date(dirs: list, 
                                     filetypes: list) -> list:
    if len(dirs) >= 1:
        f = Files(dirs[0])
    else:
        return []
    if len(dirs) >= 2:
        for i in range(1, len(dirs)):
            f.extend(Files(dirs[i]))
    f.filter_with_filetypes(filetypes)
    return f.get_list_sorted_by_date()


def copy_list_into(dest_file: str, f: list) -> None:
    f_sort_by_date = f
    html_file_list = [[], [], []]
    state = 0
    BEFORE_LIST, IN_LIST, AFTER_LIST = 0, 1, 2
    with open(dest_file, "r") as html_file:
        for line in html_file:
            if 'START-LIST' in line and state == BEFORE_LIST:
                state = IN_LIST
            elif 'END-LIST' in line and state == IN_LIST:
                state = AFTER_LIST
            html_file_list[state].append(line)
    html_file_list[IN_LIST] = ['// START-LIST\n', 'fileList = [\n']
    for i in range(len(f_sort_by_date)):
        if i == 0:
            html_file_list[IN_LIST].append(
                '    `' + f_sort_by_date[i] + '`\n')
        else:
            html_file_list[IN_LIST].append(
                '    , `' + f_sort_by_date[i] + '`\n')
    html_file_list[IN_LIST].append(']\n') 
    with open(dest_file, "w") as html_file:
        for i in range(len(html_file_list)):
            for line in html_file_list[i]:
                try:
                    html_file.write(line)
                except UnicodeEncodeError as e:
                    print(e, "\n", line)


if __name__ == "__main__":
    f = []
    # for i in range(len(f)):
    #     filenamelist = f[i].split("/")
    #     filenamelist = filenamelist[3:]
    #     f[i] = "./" + "/".join(filenamelist)
    # print(f)
    copy_list_into("./index.html", f)
    copy_list_into("./scroll_files.js", f)
