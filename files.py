from files_dict import get_files_dict


class Files:
    """
    Class that encapsulates the files and information about
    their modification date and sizes in a dictionary format.
    """
    def __init__(self, file_path: str) -> None:
        """
        Initializer.
        """
        self._files_dict = get_files_dict(file_path)
        self._SIZE = 0
        self._DATE = 1

    def filter_with_filetype(self, filetype: str) -> None:
        """
        Filter the files to only one filetype and nothing else.
        """
        self._files_dict = {file_name: self._files_dict[file_name]
                            for file_name in self._files_dict.keys() if
                            file_name.endswith(filetype)}

    def filter_with_filetypes(self, filetypes: list) -> None:
        """
        Filter the files with the given list of filetypes.
        """
        self._files_dict = {file_name: self._files_dict[file_name]
                            for file_name in self._files_dict.keys() if
                            any([file_name.endswith(filetype)
                                 for filetype in filetypes])}

    def get_list(self) -> list:
        """
        Obtain a list of the files.
        """
        return list(self._files_dict.keys())

    def get_list_sorted_by_date(self) -> list:
        """
        Get the list of the files sorted by date.
        """
        lst = []
        for file_name in self._files_dict.keys():
            file_date = self._files_dict[file_name][self._DATE]
            lst.append([file_name, file_date])
            for i in range(len(lst) - 1, 0, -1):
                if lst[i][1] < lst[i - 1][1]:
                    lst[i], lst[i - 1] = lst[i - 1], lst[i]
        return [item[0] for item in lst]

    def get_list_sorted_by_size(self) -> list:
        """
        Get the list of the files sorted by size.
        """
        lst = []
        for file_name in self._files_dict.keys():
            file_size = self._files_dict[file_name][self._SIZE]
            lst.append([file_name, file_size])
            for i in range(len(lst) - 1, 0, -1):
                if lst[i][1] < lst[i - 1][1]:
                    lst[i], lst[i - 1] = lst[i - 1], lst[i]
        return [item[0] for item in lst]

    def extend(self, files) -> None:
        """
        Extend this files with another group of files.
        """
        for file in files._files_dict.keys():
            self._files_dict[file] = files._files_dict[file]


if __name__ == "__main__":
    f = Files("./test_date_dir")
    f.filter_with_filetype(".txt")
    print(f.get_list_sorted_by_date())
