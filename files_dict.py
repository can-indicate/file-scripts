import os


def _get_files_dict(file_path: str, files_dict: dict) -> None:
    """
    Helper function for get_files_dict.
    """
    for file in os.scandir(file_path):
        file_name = "%s%s" % (file_path, file.name)
        file_stat = os.stat(file_name)
        date_modified = file_stat.st_mtime
        file_size = file_stat.st_size
        if not os.path.isdir(file_name):
            files_dict[file_name] = [file_size, date_modified]
        else:
            file_name = "%s%s" % (file_name, "/")
            _get_files_dict(file_name, files_dict)


def get_files_dict(file_path: str) -> dict:
    """
    Return a dictionary where each key is the absolute
    path of the file, and the item is the date of the file
    and its size in memory.
    """
    if file_path[0] == "~":
        file_path = file_path.replace("~", os.environ['HOME'])
    file_path = "%s/" % file_path if file_path[-1] != "/" else file_path
    file_dict = {}
    _get_files_dict(file_path, file_dict)
    return file_dict


if __name__ == "__main__":
    d = get_files_dict("../")
    d = {file_name: d[file_name]
         for file_name in d.keys() if file_name.endswith(".py")}
    print(d)
