import os


def _list_files(file_path: str, file_list: list) -> None:
    """
    Helper function for list_files.
    """
    for file in os.scandir(file_path):
        file_name = "%s%s" % (file_path, file.name)
        if not os.path.isdir(file_name):
            file_list.append(file_name)
        else:
            _list_files(file_name, file_list)


def list_files(file_path: str) -> list:
    """
    List files recursively within a directory.
    """
    if file_path[0] == "~":
        file_path = file_path.replace("~", os.environ['HOME'])
    file_path = "%s/" % (file_path) if file_path[-1] != "/" else file_path
    file_list = []
    _list_files(file_path, file_list)
    return file_list


if __name__ == "__main__":
    print(list(
            filter(lambda string: string.endswith(".pdf"),
                   list_files("../"))))

