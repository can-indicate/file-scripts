from files import Files


def get_list_of_files_sorted_by_date(dirs: list, 
                                     filetypes: list) -> list:
    if len(dirs) >= 1:
        f = Files(dirs[0])
    if len(dirs) >= 2:
        for i in range(1, len(dirs)):
            f.extend(Files(dirs[i]))
    f.filter_with_filetypes(filetypes)
    return f.get_list_sorted_by_date()


def make_index_html(f: list) -> None:
    f_sort_by_date = f
    html_file_list = [[], [], []]
    state = 0
    BEFORE_LIST, IN_LIST, AFTER_LIST = 0, 1, 2
    with open("./index.html", "r") as html_file:
        for line in html_file:
            if 'START-LIST' in line and state == BEFORE_LIST:
                state = IN_LIST
            elif 'END-LIST' in line and state == IN_LIST:
                state = AFTER_LIST
            html_file_list[state].append(line)
    html_file_list[IN_LIST] = ['// START-LIST\n', 'fileList = [\n']
    for i in range(len(f_sort_by_date)):
        if i == 0:
            html_file_list[IN_LIST].append(
                '    `' + f_sort_by_date[i] + '`\n')
        else:
            html_file_list[IN_LIST].append(
                '    , `' + f_sort_by_date[i] + '`\n')
    html_file_list[IN_LIST].append(']\n') 
    with open("./index.html", "w") as html_file:
        for i in range(len(html_file_list)):
            for line in html_file_list[i]:
                html_file.write(line)


if __name__ == "__main__":
    # for i in range(len(f)):
    #     filenamelist = f[i].split("/")
    #     filenamelist = filenamelist[3:]
    #     f[i] = "./" + "/".join(filenamelist)
    # print(f)
    make_index_html([])
