import sys
import webbrowser
from http import server
from socketserver import TCPServer
import threading
import random


class StartBrowserThread(threading.Thread):

    def run(self):
        webbrowser.get().open("http://localhost:%d" % port)


if __name__ == "__main__":
    port = int(random.random()*50000) + 1023
    if len(sys.argv) == 1:  
        thread = StartBrowserThread()
        thread.start()
        RequestHandler = server.SimpleHTTPRequestHandler 
        RequestHandler.extensions_map.update({".js": "application/javascript"})
        simple_server = TCPServer(("localhost", port), RequestHandler)
        simple_server.serve_forever()
