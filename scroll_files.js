let index = 0;
let indexSlider = null;
let indexSliderLabel = null;
let aList = [];
let useRandom = false;
document.addEventListener('DOMContentLoaded', main);


function incrementIndex() {
    index += 1;
    if (indexSlider !== null && indexSliderLabel !== null) {
        indexSliderLabel.textContent = `index=${index}`;
        indexSlider.value = index;
    }
}


function setIndex(newIndex) {
    index = newIndex;
    if (indexSlider !== null && indexSliderLabel !== null) {
        indexSliderLabel.textContent = `index=${index}`;
        indexSlider.value = index;
    }
}


function addDiv(elemEmbed, newDiv) {
    elemEmbed.innerHTML += `<div>` + newDiv + `</div>`;
    elemEmbed.innerHTML += `<div style=
    "height: ${Math.floor(0.1*elemEmbed.clientHeight)}; "></div>`;
}


function replaceDiv(elemEmbed, newDiv) {
    elemEmbed.innerHTML = `<div>` + newDiv + `</div>`;
    elemEmbed.innerHTML = `<div style=
    "height: ${Math.floor(0.1*elemEmbed.clientHeight)}; "></div>`;
}

function clearDiv(elemEmbed) {
    elemEmbed.innerHTML = '';
}


function addDivFromList(elemEmbed, elemList, random=false) {
    if (index >= 0 && index < elemList.length) {
        if (random) {
            let randomIndex = Math.round(
                Math.random()*(elemList.length - 1));
            addDiv(elemEmbed, elemList[randomIndex]);
            setIndex(randomIndex);
        } else {
            addDiv(elemEmbed, elemList[index]);
            incrementIndex();
        }
    }
}


function replaceDivFromList(elemEmbed, elemList, random=false) {
    if (index >= 0 && index < elemList.length) {
        if (random) {
            let randomIndex = Math.round(
                Math.random()*(elemList.length - 1));
            replaceDiv(elemEmbed, elemList[randomIndex]);
            setIndex(randomIndex);
        } else {
            replaceDiv(elemEmbed, elemList[index]);
            incrementIndex();
        }
    }
}


/*
    To get endless scrolling to work, the
    following StackExchange answer is referred to
    https://stackoverflow.com/a/44070803 
    (by Vishaa https://stackoverflow.com/users/7651278/vishaa)
    (Original question https://stackoverflow.com/q/22268079
    by Arnold https://stackoverflow.com/users/1080012/arnold)
*/
function main() {
    let scroll = document.getElementById("scroll-media");
    scroll.width = 0.99*document.clientWidth;
    scroll.height = 0.99*document.clientHeight;
    // addDiv(scroll, `<div style="height: 2*${scroll.clientHeight}"> </div>`)
    addDivFromList(scroll, aList, useRandom);
    addDivFromList(scroll, aList, useRandom);
    let randomCheckbox = document.getElementById("random");
    randomCheckbox.addEventListener("click", () => useRandom = !useRandom);
    let refreshButton = document.getElementById("refresh");
    refreshButton.addEventListener("click", () => clearDiv(scroll));
    indexSlider = document.getElementById("index");
    indexSliderLabel = document.getElementById("index-label");
    indexSlider.min = 0;
    indexSlider.max = aList.length - 1;
    indexSlider.addEventListener("mouseup", () => {
        index = parseInt(indexSlider.value);
        indexSliderLabel.textContent = `index=${index}`;
        // console.log("slider changed", indexSlider.value);
    });
    randomCheckbox.checked = (useRandom)? true: false; 
    function scrollCallback () {
        if (scroll.scrollHeight - scroll.scrollTop == scroll.clientHeight) {
            addDivFromList(scroll, aList, useRandom);
        }
    }    
    scroll.addEventListener("scroll", scrollCallback);
}


// START-LIST
fileList = [
]
// END-LIST


for (var i = 0; i < fileList.length; i++) {
    if (fileList[i].endsWith(".webm") || fileList[i].endsWith(".mp4")) {
        aList.push(`<video src=\"${fileList[i]}\" 
            controls style=\"height: 512\"/></video>`
            );
    } else if (fileList[i].endsWith(".jpg") || fileList[i].endsWith(".JPG") ||
            fileList[i].endsWith(".png") || fileList[i].endsWith(".PNG") ||
            fileList[i].endsWith(".gif") || fileList[i].endsWith(".GIF")) {
        aList.push(`<a href=\"${fileList[i]}\">
            <img src=\"${fileList[i]}\" style=\"height: 512\" /></a>`
            );
    } else if (fileList[i].endsWith(".pdf")) {
        aList.push(`<embed src="${fileList[i]}" width="1024" height="512"
                    type="application/pdf" />`);
    }
}
aList.reverse();
